Warp10 Mod v3 Changelog
==============================================

Added:
--------


Shipsets: Borg, Federation, Klingon, Romulan

Changed:
--------
Merged Crew Quarters and Life Support into one component "Crew Support" which is basically the sum of both their stats
Rock/Ice/Gas changed to Class-M/Class-P/Class-H, allowed Class-H/None combination
Bridges and Control/Master Computers now allow a ship to self-destruct, but their does not trigger on boarding
Nerfed Shield Imploder hard: 5x size/cost and 2x reload as it's basically a one-hit kill weapon on heavily shielded ships.
Ship model sizes reduced to mitigate silly vertical ship stacking in combat
Reduced combat arena size from 2000 units to 800
Allowed to zoom out twice as far in combat (which makes ship look smaller again)
New Shield hit graphics
Smaller combat damage animations to go with the smaller shipsets


Renamed Auxiliary Control -> Battle Bridge

Removed:
--------
Vanilla-like racial traits: Organic, Crystalline etc.
Organic and Crystalline ship hulls


Fixed:
--------
