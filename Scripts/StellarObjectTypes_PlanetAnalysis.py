# this script counts the planet physical type and atmosphere combinations in the StellarObjectTypes file.

import re

planetTypes = ["Class-M", "Class-P", "Class-H"]
planetSizes = ["Tiny", "Small", "Medium", "Large", "Huge"]
planetAtmos = ["None", "Methane", "Oxygen", "Hydrogen", "Carbon Dioxide"]

currentPlanetType = -1
currentPlanetSize = -1
currentPlanetAtmo = -1

planets = [[[0] * len(planetAtmos) for i in range(len(planetSizes))] * len(planetSizes) for i in range(len(planetTypes))]
# Type Size Atmo

rx_dict = {
    'size': re.compile(r"Planet Size\s*:=\s*(.+)\s*$"),
    'type': re.compile(r"Planet Physical Type\s*:=\s*(.+)\s*$"),
    'atmosphere': re.compile(r"Planet Atmosphere\s*:=\s*(.+)\s*$"),
}

def _parse_line(line):
    """
    Do a regex search against all defined regexes and
    return the key and match result of the first matching regex

    """

    for key, rx in rx_dict.items():
        match = rx.search(line)
        if match:
            return key, match
    # if there are no matches
    return None, None

with open("../Data/StellarObjectTypes.txt", "r") as file_object:
    # it is assumed that the three attributes are always encountered in the order listed here, always pertaining to one planet type
    line = file_object.readline()
    while line:
        key, match = _parse_line(line)
        if key == 'size':
            if match.group(1) != "Ringworld" and match.group(1) != "Sphereworld":
                i = 0
                for size in planetSizes:
                    if match.group(1) == size:
                        currentPlanetSize = i
                    i += 1
            else:
                break
        if key == "type":
            i = 0
            for type in planetTypes:
                if match.group(1) == type:
                    currentPlanetType = i
                i += 1
        if key == "atmosphere":
            i = 0
            for atmo in planetAtmos:
                if match.group(1) == atmo:
                    currentPlanetAtmo = i
                i += 1
            # add planet to list after it's three stats are known
            planets[currentPlanetType][currentPlanetSize][currentPlanetAtmo] += 1

        line = file_object.readline()

# output
i = 0
while i < len(planetTypes):
    print(planetTypes[i], end='')
    for atmo in planetAtmos:
        print('\t' + atmo, end='')
    print('\n')
    j = 0
    while j < len(planetSizes):
        print(planetSizes[j], end='')
        k = 0
        while k < len(planetAtmos):
            print('\t' + str(planets[i][j][k]), end='')
            k += 1
        print('\n')
        j += 1
    print('\n')
    i += 1
