=======================================================================================
PLANET PHYSICAL TYPES DATA FILE - Warp10 Mod v3
=======================================================================================

Abilities like "Planet Colonize Type #" take their numbering from this file, so type 1 is M, type 2 is P etc.


=======================================================================================
*BEGIN*
=======================================================================================

Name                                 := Class-M
Description                          :=
Picture Index                        := 2
Tech Area For Colonization           := Class-M Colonization

Name                                 := Class-P
Description                          :=
Picture Index                        := 1
Tech Area For Colonization           := Class-P Colonization

Name                                 := Class-H
Description                          :=
Picture Index                        := 3
Tech Area For Colonization           := Class-H Colonization

=======================================================================================
*END*
=======================================================================================
