===================================================================
Guide to Reducing a Shipset to Gurachn's Shipset Reduction Standard
===================================================================

This document is intended to help you reduce a shipset to the standard sizes
used with Gurachn's Shipset Reduction. It's easy, but a bit time-consuming.



================
Basic Principles
================

Act only in such a way as you should wish that all should act.

Wait, wrong document.

The basic principle here is that we're reducing everything half-size to begin
with, and reducing the size difference between the larger and smaller
vehicles.

What this means is that we generally start with about 50% size with frigates,
and increase the reduction proportionally as we get to larger sizes. Nothing
mathematical -- it's all eyeballing. You open the Shipset Viewer and look at
an already-reduced shipset so you have a standard to compare to, then you open
*another* instance of the Shipset Viewer and look at the one you want to
reduce. You flip back and forth looking at both and adjusting til you get it
to a comparable size.

We're not trying to match one or two dimensions -- some ships are proportioned
differently, and we want to keep the overall sense of size the same.  So some
will be considerably larger height-and-depth-wise than, say, a shipset that is
deep and bulky.

We'll go into how we do that in detail a bit below, but that's the basic gist.



===========
Bonus Round
===========

Big bucks, no whammies.

If you like, it's always a good idea to correct errors or omissions by the
original shipset maker. Hey, we all make mistakes, and some are doozies. You'd
be surprised how easy it is to accidentally leave a major oops in here --
anything from decimal points in the wrong place to ships flying backward or
sideways.

Correcting engine points can sometimes be a must, especially when they're
glaringly off. It's not too hard, once you get the hang of it. You can also
correct weapon points, or add engine and weapon points if it doesn't have any,
though these aren't as critical as misplaced engine points.

This kind of quality control is what turns a meh shipset into one that
everyone likes to use. Assuming there's a quality minimum there in other
respects.


============
Starting Out
============

Leave the two instances of the Shipset Viewer in the *exact same place* on
your desktop. Zoom in to the exact same level (you can use the grid to help
you get this right). And then when you alt-TAB between them, you should see
that they are *exactly* in the same place.

Once you do that, with each ship it's basically a function of eyeballing the
ships, then using the Shipset Viewer commands to reduce them in the viewer
itself.

NOTE THAT ANYTHING YOU DO IN THE SHIPSET VIEWER IS TEMPORARY; ANY CHANGES YOU
WANT TO DO, YOU MUST DO USING A TEXT EDITOR.

Yes, the Shipset Viewer is not *that* handy. So use a decent text editor
(ditch Notepad, will you?) and get ready to change all this manually.



=======================
Base Model Radius (BMR)
=======================

The Base Model Radius, or BMR, is used for two main purposes that I know of --
there are probably more, but these ones concern us here.

First, it influences how large the ship will show up on the main game sector
map.

Second, it controls how the game will display the little fiery jet thingies
coming out when the ship has taken substantial damage. If you've ever seen a
ship damaged, with flame jets coming out from some place not even connected to
the ship, and likely too far away from it, you've seen a BMR that was set too
high.

What this means is we want to give the game a sense of the radius of the
*bulk* of the ship, not an actual radius of all its parts. Antennae, pointy
thingies, wings, etc. should be left off.

That said, here's how you actually do it.  Check the grid -- if the ship is
properly centered, it should be roughly equal in front and back.

  -= Side Note: Ship Centering and Center-of-Mass =-
  The point is to center the ship on its apparent center-mass. With many
  designs, it's perfectly normal for the front of the ship to be, say, 5
  squares past the centerline, and the back of the ship to be only 3.5. It
  just depends on where the real center-mass of the ship is. If you center it
  literally in such a case, you'll make it seem to turn weirdly in-game. On
  the other hand, sometimes they're really off and you should adjust the
  appropriate offset coordinate to center it better. Don't forget to adjust
  all the engine points and weapon points when you do this.

Find the extends of the *bulk* of the ship, again, leaving off longer bits. If
the ship is not centered exactly (say, our 5.0 forward and 3.5 behind
example), average them.  So our example would be:

(5.0 + 3.5 = 8.5) / 2 = BMR of 4.3 or so

It's not exact by any stretch.  My own take on it is that if you're going to
make an error, err on the side of slightly too small rather than slightly too
big.


================
Explosion Radius
================

Explosion radius is fortunately much quicker and simpler than ship radius.

Originally, we were determining this by the simple formula of Base Radius x 3.
However, since we're going for roughly the same size ships of each class, and
ship proportions vary (some, for example, will have a smaller radius than
other races' ships of the same class because they're very bulk, or are tall),
it seems to make more sense to just assign uniform values to each class of
ship.

So, instead of going to the trouble of such calculations when our results are
based on such a high degree of imprecise estimation, we just give them the
following values uniformly:

Space Battle Explosions

  Frigate              5.0
  Destroyer            6.5
  Light Cruiser        9.0
  Cruiser             12.0
  Battleship          15.0
  Dreadnought         18.0
  Baseship            22.0

  Light Carrier       10.0
  Carrier             13.0
  Heavy Carrier       16.0

  Colony Ship          7.0
  Small Freighter      6.0
  Medium Freighter     9.0
  Large Freighter     12.0

  Space Station       28.0
  Starbase            45.0

  Drone                3.0


Ground Battle Explosions

  (In space battles, fighters, satellites and mines use tiny sparkle-
  explosions that do not vary in size. They don't use the explosion radius
  values to my knowledge. Thus, the following will only show up in ground
  battles. Except for satellites and mines -- I don't think mine or satellite
  explosions radii are used anywhere, but the entries are there, and just in
  case, let's keep them consistent.)

Fighter             8.0
Mine                6.0 <-- it's an explosive, so should be bigger!
Satellite           3.5
(Small Troop)       4.5 <-- most shipsets don't have these entries
Troop               6.0
(Large Troop)       8.0 <-- most shipsets don't have these entries
Weapon Platform    10.0
