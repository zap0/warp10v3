=======================================================================================
AI MAIN DATA FILE
=======================================================================================


=======================================================================================
*BEGIN*
=======================================================================================

Is Empire Race                                := TRUE
Is Available For Player Selection             := TRUE
Is Neutral Race                               := FALSE
Empire Name                                   := Borg
Image Empire Flag Set                         := Borg\Borg_Flag_Set.bmp
Image Empire Flag Main Texture                := Borg\Borg_Flag_Texture.bmp
Image Empire Flag Ship Texture                := Borg\Borg_Flag_Texture_Ship.bmp
Image Empire Flag Fleet Texture               := Borg\Borg_Flag_Texture_Fleet.bmp
Image Race Smaller Portraits                  := Borg\Borg_Race_SmallPortraits.bmp
Image Race Medium Portrait                    := Borg\Borg_Race_Portrait.bmp
Image Race Large Portrait                     := Borg\Borg_Race_LargePortrait.jpg
Image Vehicle Main Texture                    := Borg\Borg_Vehicle_Main_Texture.bmp
Image Unit Main Texture                       := Borg\Borg_Unit_Main_Texture.bmp
Image Unit Combat Texture                     := Borg\Borg_Unit_Combat_Texture.bmp
Image Topdown Shipset                         := Borg\Borg_ShipSet.bmp
AI Script File                                := Default\Default_Aggressive_Main_Script.csf
AI Script Empire Setup File                   := Default\Default_Aggressive_Setup_Script.csf
AI General File                               := Borg\Borg_AI_General.txt
AI Ministers File                             := Default\Default_AI_Ministers.txt
AI Speech File                                := Borg\Borg_AI_Speech.txt
AI Strategies File                            := Default\Default_AI_Strategies.txt
AI Warp Transit Types File                    := Default\Default_AI_WarpTransitTypes.txt
Inventory Configuration Slots File            := Borg\Borg_InvConfiguration_Slots.txt
Ship XFile Classes File                       := Borg\Borg_Ships_XFileClasses.txt
Design Name File                              := Borg\Borg_AI_DesignNames.txt

=======================================================================================
*END*
=======================================================================================

