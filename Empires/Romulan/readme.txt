

Star Trek - Romulan (TNG Era) Ship Set.

Set version:		V1.02	
Release Date:		1-30-2007
Generated by:		Atrocities 
Email:			atroicites@gmail.com

======================
Installation:
======================

Place this ship set in your Empires folder and it will become avalible for play, no AI has been included, it is strictly a ship set.  

======================
Thanks Go to:
======================

Race Portrait:		Unknown
Emblem:			Microprose (BOTF)

Flags:			David Gervais
Slot Lay Out:		President Elect Shang
SE V Models:		Malfador/SFI 
Top Down Slot Images:	Atrocities - using DOGA L3 Modeler

Ship Models:
colony ship	by:	Thu11s & Atrahasis
Frigate  	by: 	Microprose (BOTF)
Destroyer	by: 	Unknown
Light CR	by: 	Ratman
Cruiser		by: 	Amaral
Battleship	by: 	Tyrel Lohr
Dreadnought	by:	Dawn
Carrier		by:	Microprose (BOTF)
Friegter	by:	Aenigma	
Space Station	by:	Unknown
Starbase	by:	Unknown
		

Additonal Thanks and Credit to:
Activision
Interplay
Taldren
Chris Jones 
Pneumonic81
Microprose (BOTF)

======================
Special Thanks To:
======================

Aaron Hall 	for a wonderful game!!!
Necromancer3	for getting the ball rolling
David Gervais	for his wonderful art work - Flags.
shinigami 	for his help 
Suicide Junkie	for his help
PE Shang	for his help
Fyron		for his help
Kwok		for his help
Shrapnel Games	for allowing me to post this on their forum
SE.net		for supporting the SE V community (www.spaceempires.net)
SFI		for publishing the SE V.


======================
Revisions:
======================
1.0 - Original Release
1.01 - Replaced wrong sized race portrait.
1.02 - REPLACED wrong sized race portrait again.


======================
Disclaimers:
======================

I am in no way claiming ownership of any files located in this mod other than this readme file and any additional installation instructions (if provided). 

I cannot be held responsible for any damage, loss of data, or corruption of files caused by the use of this mod. It is recommended that all files are scanned for viruses before use. 

I wish to stress that this Ship Set was made by a 3rd party author and that I have played no role whatsoever in The construction of the ships included here in. 
