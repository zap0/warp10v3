Klingon_frigate.X =============================== 
  Datafile Base Model Radius: 3.0000
  Scale:            0.3200, 0.3200, 0.3200
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:     0.0000,     0.0000,     4.7500,       (340.00 - 20.00)
    Firing Point 2:    -5.5000,    -2.0000,     2.6000,       (280.00 - 80.00)
    Firing Point 3:     5.5000,    -2.0000,     2.6000,       (280.00 - 80.00)
  Engine Points - 
    Engine Point 1:     0.0000,     1.0000,    -4.5000,       (gr=0.30)
Klingon_destroyer.X =============================== 
  Datafile Base Model Radius: 4.2000
  Scale:            0.3000, 0.3000, 0.3000
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:     0.0000,     0.0000,    15.0000,       (280.00 - 80.00)
    Firing Point 2:     4.0000,     1.0000,    -1.8000,       (280.00 - 80.00)
    Firing Point 3:    -4.0000,     1.0000,    -1.8000,       (280.00 - 80.00)
    Firing Point 4:     0.0000,     1.0000,   -10.0000,       (135.00 - 225.00)
  Engine Points - 
    Engine Point 1:     1.5000,     1.0000,   -11.0000,       (gr=0.40)
    Engine Point 2:    -1.5000,     1.0000,   -11.0000,       (gr=0.40)
Klingon_lightcruiser.X =============================== 
  Datafile Base Model Radius: 5.5000
  Scale:            0.3000, 0.3000, 0.3000
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:     0.0000,     0.5000,    18.0000,       (315.00 - 45.00)
    Firing Point 2:     0.0000,     2.2000,    13.5000,       (270.00 - 90.00)
    Firing Point 3:     0.0000,     3.0000,   -12.0000,       (90.00 - 270.00)
    Firing Point 4:     0.0000,     0.7000,   -15.2000,       (90.00 - 270.00)
    Firing Point 5:     0.0000,     2.8000,     0.9000,       (270.00 - 90.00)
  Engine Points - 
    Engine Point 1:     5.8000,     0.5000,   -15.6000,       (gr=0.50)
    Engine Point 2:    -5.8000,     0.5000,   -15.6000,       (gr=0.50)
Klingon_cruiser.X =============================== 
  Datafile Base Model Radius: 6.0000
  Scale:            0.2000, 0.2000, 0.2000
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:     0.0000,    -0.5000,    34.2000,       (270.00 - 90.00)
    Firing Point 2:     0.0000,     5.0000,   -10.0000,       (270.00 - 90.00)
    Firing Point 3:     0.0000,     0.4000,   -37.0000,       (90.00 - 270.00)
  Engine Points - 
    Engine Point 1:    33.0000,    -9.0000,   -21.0000,       (gr=0.40)
    Engine Point 2:   -33.0000,    -9.0000,   -21.0000,       (gr=0.40)
    Engine Point 3:     0.0000,    -9.0000,   -21.0000,       (gr=0.40)
Klingon_battleship.X =============================== 
  Datafile Base Model Radius: 8.8000
  Scale:            0.3000, 0.3000, 0.3000
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:     0.0000,     0.0000,    21.2000,       (270.00 - 90.00)
    Firing Point 2:     0.0000,     0.7000,   -22.9000,       (90.00 - 270.00)
    Firing Point 3:     0.0000,     2.5000,    12.1000,       (280.00 - 80.00)
    Firing Point 4:     0.0000,     6.0000,   -12.3000,       (270.00 - 90.00)
  Engine Points - 
    Engine Point 1:     4.5000,     0.0000,   -23.0000,       (gr=0.50)
    Engine Point 2:    -5.0000,     0.0000,   -23.0000,       (gr=0.50)
Klingon_dreadnought.X =============================== 
  Datafile Base Model Radius: 15.0000
  Scale:            0.3500, 0.3500, 0.3500
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:     0.0000,     0.0000,    37.0000,       (270.00 - 90.00)
    Firing Point 2:     9.0000,     0.0000,    30.0000,       (10.00 - 170.00)
    Firing Point 3:    -9.0000,     0.0000,    30.0000,       (190.00 - 350.00)
    Firing Point 4:     0.0000,     7.0000,   -37.0000,       (100.00 - 260.00)
    Firing Point 5:     0.0000,     6.0000,   -14.0000,       (225.00 - 135.00)
    Firing Point 6:     9.1000,    -5.0000,    -0.4000,       (315.00 - 45.00)
    Firing Point 7:    -9.1000,    -5.0000,    -0.4000,       (315.00 - 45.00)
    Firing Point 8:     9.2000,    -5.0000,   -22.3000,       (135.00 - 225.00)
    Firing Point 9:    -9.2000,    -5.0000,   -22.3000,       (135.00 - 225.00)
  Engine Points - 
    Engine Point 1:    13.0000,    -1.5000,   -29.0000,       (gr=0.60)
    Engine Point 2:   -13.0000,    -1.5000,   -29.0000,       (gr=0.60)
Klingon_carrier.X =============================== 
  Datafile Base Model Radius: 9.0000
  Scale:            0.3000, 0.3000, 0.3000
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:     0.0000,    -1.5000,    26.0000,       (280.00 - 80.00)
    Firing Point 2:     0.0000,     8.0000,   -20.0000,       (1.00 - 359.00)
    Firing Point 3:     2.5000,     2.5000,    24.0000,       (5.00 - 175.00)
    Firing Point 4:    -2.5000,     2.5000,    24.0000,       (185.00 - 350.00)
  Engine Points - 
    Engine Point 1:     0.0000,     0.0000,   -25.0000,       (gr=0.80)
Klingon_colonyship.X =============================== 
  Datafile Base Model Radius: 6.0000
  Scale:            0.3500, 0.3500, 0.3500
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:     1.0000,     2.8000,    23.7000,       (280.00 - 80.00)
    Firing Point 2:    -1.0000,     2.8000,    23.7000,       (280.00 - 80.00)
    Firing Point 3:     0.0000,     1.0000,   -22.6000,       (90.00 - 270.00)
  Engine Points - 
    Engine Point 1:     3.0000,     2.0000,   -23.0000,       (gr=0.50)
    Engine Point 2:    -3.0000,     2.0000,   -23.0000,       (gr=0.50)
Klingon_freighter.X =============================== 
  Datafile Base Model Radius: 4.5000
  Scale:            0.3000, 0.3000, 0.3000
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:     0.0000,     1.0000,    46.0000,       (315.00 - 45.00)
    Firing Point 2:     0.0000,     6.0000,   -42.0000,       (135.00 - 225.00)
  Engine Points - 
    Engine Point 1:     0.0000,     0.0000,   -40.0000,       (gr=0.80)
Klingon_spacestation.X =============================== 
  Datafile Base Model Radius: 14.0000
  Scale:            0.1000, 0.1000, 0.1000
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:    70.0000,     0.0000,    43.0000,       (1.00 - 359.00)
    Firing Point 2:   -70.0000,     0.0000,    43.0000,       (1.00 - 359.00)
    Firing Point 3:     0.0000,     0.0000,   -85.0000,       (1.00 - 359.00)
  Engine Points - 
Klingon_starbase.X =============================== 
  Datafile Base Model Radius: 28.0000
  Scale:            0.0300, 0.0300, 0.0300
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:     0.0000,   120.0000,   200.0000,       (270.00 - 90.00)
    Firing Point 2:     0.0000,   120.0000,  -200.0000,       (90.00 - 270.00)
    Firing Point 3:   200.0000,   120.0000,     0.0000,       (0.00 - 180.00)
    Firing Point 4:  -200.0000,   120.0000,     0.0000,       (180.00 - 0.00)
  Engine Points - 
Klingon_drone.X =============================== 
  Datafile Base Model Radius: 1.4000
  Scale:            0.0100, 0.0100, 0.0100
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:    -0.4459,   118.5862,     0.1542,       (315.00 - 45.00)
  Engine Points - 
    Engine Point 1:     0.1799,  -117.0500,     0.0680,       (gr=0.30)
Klingon_fighter.X =============================== 
  Datafile Base Model Radius: 2.8000
  Scale:            0.0200, 0.0200, 0.0200
  Offset Pos:       0.0000, 0.0000, 0.2000
  Firing Points - 
    Firing Point 1:    -0.4459,    85.2998,   -20.0000,       (315.00 - 45.00)
  Engine Points - 
    Engine Point 1:   -11.9160,   -96.0000,     0.0000,       (gr=0.70)
    Engine Point 2:    12.5391,   -96.0000,     0.0000,       (gr=0.70)
Klingon_mine.X =============================== 
  Datafile Base Model Radius: 1.6000
  Scale:            0.0100, 0.0100, 0.0100
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
  Engine Points - 
Klingon_satellite.X =============================== 
  Datafile Base Model Radius: 1.8000
  Scale:            0.0100, 0.0100, 0.0100
  Offset Pos:       0.1000, 0.0000, 4.0000
  Firing Points - 
    Firing Point 1:     8.0000,   112.4825,   -50.0000,       (270.00 - 90.00)
    Firing Point 2:    10.0000,  -112.9558,   -50.0000,       (90.00 - 270.00)
    Firing Point 3:   120.0000,     0.4303,   -50.0000,       (0.00 - 180.00)
    Firing Point 4:  -100.0000,     0.4303,   -50.0000,       (180.00 - 0.00)
  Engine Points - 
Klingon_troop.X =============================== 
  Datafile Base Model Radius: 2.8000
  Scale:            0.0200, 0.0200, 0.0200
  Offset Pos:       0.0000, 0.0000, 0.0000
  Firing Points - 
    Firing Point 1:    -0.4459,    72.0000,   -35.0000,       (315.00 - 45.00)
    Firing Point 2:    33.0891,    47.0000,   -30.0000,       (315.00 - 45.00)
    Firing Point 3:   -33.0860,    47.0000,   -30.0000,       (315.00 - 45.00)
  Engine Points - 
    Engine Point 1:     0.1799,  -120.0000,   -10.0000,       (gr=0.50)
    Engine Point 2:   -85.6671,   -60.0000,     0.0000,       (gr=0.40)
    Engine Point 3:    85.0974,   -60.0000,     0.0000,       (gr=0.40)
Klingon_weaponplatform.X =============================== 
  Datafile Base Model Radius: 5.4000
  Scale:            0.0260, 0.0260, 0.0260
  Offset Pos:       0.0000, 0.0000, -2.4000
  Firing Points - 
    Firing Point 1:    -1.6641,     0.4181,  -176.3906,       (0.00 - 0.00)
  Engine Points - 
