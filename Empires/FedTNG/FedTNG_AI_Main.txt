=======================================================================================
AI MAIN DATA FILE
=======================================================================================


=======================================================================================
*BEGIN*
=======================================================================================

Is Empire Race                                := TRUE
Is Available For Player Selection             := TRUE
Is Neutral Race                               := FALSE
Empire Name                                   := FedTNG
Image Empire Flag Set                         := FedTNG\FedTNG_Flag_Set.bmp
Image Empire Flag Main Texture                := FedTNG\FedTNG_Flag_Texture.bmp
Image Empire Flag Ship Texture                := FedTNG\FedTNG_Flag_Texture_Ship.bmp
Image Empire Flag Fleet Texture               := FedTNG\FedTNG_Flag_Texture_Fleet.bmp
Image Race Smaller Portraits                  := FedTNG\FedTNG_Race_SmallPortraits.bmp
Image Race Medium Portrait                    := FedTNG\FedTNG_Race_Portrait.bmp
Image Race Large Portrait                     := FedTNG\FedTNG_Race_LargePortrait.jpg
Image Vehicle Main Texture                    := FedTNG\FedTNG_Vehicle_Main_Texture.bmp
Image Unit Main Texture                       := FedTNG\FedTNG_Unit_Main_Texture.bmp
Image Unit Combat Texture                     := FedTNG\FedTNG_Unit_Combat_Texture.bmp
Image Topdown Shipset                         := FedTNG\FedTNG_ShipSet.bmp
AI Script File                                := Default\Default_Defensive_Main_Script.csf
AI Script Empire Setup File                   := Default\Default_Defensive_Setup_Script.csf
AI General File                               := FedTNG\FedTNG_AI_General.txt
AI Ministers File                             := Default\Default_AI_Ministers.txt
AI Speech File                                := FedTNG\FedTNG_AI_Speech.txt
AI Strategies File                            := Default\Default_AI_Strategies.txt
AI Warp Transit Types File                    := Default\Default_AI_WarpTransitTypes.txt
Inventory Configuration Slots File            := FedTNG\FedTNG_InvConfiguration_Slots.txt
Ship XFile Classes File                       := FedTNG\FedTNG_Ships_XFileClasses.txt
Design Name File                              := FedTNG\FedTNG_AI_DesignNames.txt

=======================================================================================
*END*
=======================================================================================
