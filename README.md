Warp10 Mod v3 for Space Empires V - A Star Trek Mod
==============================================


Contents:
---------

1.  Description
2.  Links


1. Description:
--------------

The Warp10 Mod for Space Empires V is a Star Trek themed Mod based on the Balance Mod.

Warp10 Mod version 1 was originally conceived by Ekolis and then continued
by Marhawkman in version 2.
This is an attempt at a complete rewrite of the mod on top of a more modern version
of Balance Mod, mainly to take advantage of the new inclusion of Small Systems
in Balance Mod, which helps processing times in long and/or large games.

This is not yet a playable mod!


2. Links
--------------

Balance Mod:
http://www.captainkwok.net/balancemod.php

Spaceempires.net:
http://spaceempires.net/

Play By Web (including functioning Forum):
http://pbw.spaceempires.net/

Space Empires Discord:
https://discord.gg/2c4AV77
